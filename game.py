# Importing random number library
from random import randint

# Ask for name
name = input("Hi! What is your name?")

for guess_number in range(1,6):

    month_guess = randint(1, 12)
    year_guess = randint(1924, 2004)

    # Show guess
    print(name, "is your birthday", month_guess, "/", year_guess , "?")

    response = input("Y/N?")

    if response == "Y" or response == "y":
        print("Awww yeaa I knew it! Now get outta here!!!!")
        exit()
    elif guess_number == 5:
        print("I'm sick of this! Get outta here!!!!")
    else:
        print("Hmmm...Let me try again!")
